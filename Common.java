import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Common {
    public static List<Object> value() {
        int allTreasures = 0;
        int[][] value = null;
        int startX = 1, startY = 1;
        //you can use relative path or full path here
        File file = new File("labyrinths/labyrinth_9.txt");
        try {
            Scanner sizeScanner = new Scanner(file);
            String[] temp = sizeScanner.nextLine().split(",");
            sizeScanner.close();
            int nMatrix = temp.length;

            Scanner scanner = new Scanner(file);
            value = new int[nMatrix][nMatrix];
            for (int i = 0; i < nMatrix; i++) {
                String[] numbers = scanner.nextLine().split(",");
                for (int j = 0; j < nMatrix; j++) {

                    value[i][j] = Integer.parseInt(numbers[j]);
                    if(value[i][j] == -3)
                        allTreasures++;
                    if(value[i][j] == -2){
                        startX = j;
                        startY = i;
                    }
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Arrays.asList(value, allTreasures, startY, startX);
    }

    public static class Node {
        public int x;
        public int y;
        Node parent;
        public int originalPrice;
        // Boolean visited = false;

        public Node(int x, int y, Node parent, int originalPrice) {
            this.x = x;
            this.y = y;
            this.parent = parent;
            this.originalPrice = originalPrice;
        }

        public Node getParent() {
            return this.parent;
        }

        public String toString() {
            return "x = " + x + " y = " + y;
        }
    }

    public static boolean isFree(int x, int y, int[][] graphMatrix, boolean[][] visited) {
        //System.out.println(graphMatrix[x][y]);
        return x < graphMatrix.length &&
                y < graphMatrix[x].length &&
                (isTreasure(x, y, graphMatrix) || isEnd(x, y,graphMatrix) || isHallway(x, y, graphMatrix)) &&
                !visited[x][y];
    }

    public static boolean isTreasure(int x, int y, int[][] graphMatrix){
        return graphMatrix[x][y] == -3;
    }

    public static boolean isEnd(int x, int y, int[][] graphMatrix) {
        return graphMatrix[x][y] == -4;
    }

    public static boolean isHallway(int x, int y, int[][] graphMatrix) {
        return graphMatrix[x][y] >= 0;
    }

    public static void printFinalPath(Common.Node n, int startX, int startY, int visitedNodes) {
        int price = 0;
        int length = 2; //upošteva se še začetno in končno vozlišče

        System.out.println("Path:");
        System.out.println("x = " + n.y + " y = " + n.x);
        while(n.getParent().getParent() != null) {
            System.out.print("x = " + n.getParent().y + " y = " + n.getParent().x);
            if (n.getParent().originalPrice > 0) { // polja, ki niso hodniki imajo ceno 0 in se ne prištevajo
                price += n.getParent().originalPrice;
                //System.out.print(", " + n.getParent().originalPrice);
            }
            System.out.println();
            length++;
            n = n.getParent();
        }
        System.out.println("x = " + startX + " y = " + startY);
        System.out.println("\nPrice: " + price);
        System.out.println("Length: " + length);
        System.out.println("Number of visited nodes: " + visitedNodes);
    }

    public static void markPath(Common.Node n, int[][] graphMatrix) {
        while(n.getParent().getParent() != null) {
            n = n.getParent();
            graphMatrix[n.x][n.y] = 0;
        }
    }

    public static void drawTreasures(Common.Node n, int[][] graphMatrix) {
        int[][] value = (int[][]) Common.value().get(0);
        for (int i = 0; i < graphMatrix.length; i++) {
            for (int j = 0; j < graphMatrix[i].length; j++) {
                if(value[i][j] == -3)
                    graphMatrix[i][j] = value[i][j];
            }
        }
    }

    public static void resetVisited(boolean[][] visitedMatrix, int[][] graphMatrix) {
        visitedMatrix = new boolean[graphMatrix.length][graphMatrix[0].length];
    }

}
