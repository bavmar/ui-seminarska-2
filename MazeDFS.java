import java.util.*;

public class MazeDFS {

    static int[][] originalMatrix;
    static int[][] graphMatrix;
    static boolean[][] visitedMatrix;

    static int startX, startY;
    static int remainingTreasures;
    static int visitedNodes = 0;
    public static Stack<Common.Node> s = new Stack<>();

    public static void resetVisited() {
        visitedMatrix = new boolean[graphMatrix.length][graphMatrix[0].length];
    }

    public static ArrayList<Common.Node> getPathDFS(int x, int y) {
        ArrayList<Common.Node> targetNodes = new ArrayList<>();

        // ustvarimo začetno vozlišče in ga dodamo na stack
        s.add(new Common.Node(x, y, null, originalMatrix[x][y]));

        while(!s.isEmpty()) {
            Common.Node n = s.pop();
            visitedMatrix[n.x][n.y] = true;
            visitedNodes++;

            if (Common.isTreasure(n.x, n.y, graphMatrix)) {
                graphMatrix[n.x][n.y] = 4;
                resetVisited();
                visitedMatrix[n.x][n.y] = true;
                remainingTreasures--;
                s.clear();
                s.add(n);
                targetNodes.add(n); // da lahko izrišemo tudi nekončna vozlišča
                Common.markPath(n, graphMatrix);
                //System.out.println("Remaining treasures: " + remainingTreasures);
            }

            if (Common.isEnd(n.x, n.y, graphMatrix) && remainingTreasures == 0) {
                //System.out.println("Exit is reached!");
                Common.markPath(n, graphMatrix);
                targetNodes.add(n);
                return targetNodes;
            }

            //pogleda vozlišče na desni
            if(Common.isFree(n.x+1, n.y, graphMatrix, visitedMatrix)) {
                s.add(new Common.Node(n.x+1, n.y, n, originalMatrix[n.x+1][n.y]));
            }

            //pogleda vozlišče na levi
            if(Common.isFree(n.x-1, n.y, graphMatrix, visitedMatrix)) {
                s.add(new Common.Node(n.x-1, n.y, n, originalMatrix[n.x-1][n.y]));
            }

            //pogleda vozlišče pod
            if(Common.isFree(n.x, n.y+1, graphMatrix, visitedMatrix)) {
                s.add(new Common.Node(n.x, n.y+1, n, originalMatrix[n.x][n.y+1]));
            }

            //pogleda vozlišče nad
            if(Common.isFree(n.x, n.y-1, graphMatrix, visitedMatrix)) {
                s.add(new Common.Node(n.x, n.y-1, n, originalMatrix[n.x][n.y-1]));
            }
            //System.out.println("Current position: " + n);
        }
        return null;
    }

    public static void main(String[] args) {
        List<Object> com = Common.value();
        graphMatrix = (int[][]) com.get(0);
        remainingTreasures = (int) com.get(1);
        startX = (int) com.get(2);
        startY = (int) com.get(3);

        originalMatrix = (int[][]) Common.value().get(0);

        resetVisited();
        ArrayList<Common.Node> targetNodes = getPathDFS(startX, startY);
        targetNodes.forEach(n -> Common.drawTreasures(n, graphMatrix));
        Common.printFinalPath(targetNodes.get(targetNodes.size() - 1), startX, startY, visitedNodes);
        Izris.drawOut(graphMatrix);
    }
}