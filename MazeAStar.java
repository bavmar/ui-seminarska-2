import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

public class MazeAStar {

    static int[][] originalMatrix;
    static int[][] graphMatrix;
    static boolean[][] visitedMatrix;
    static int[][] pathMatrix;

    static int startX, startY, endX, endY;;
    static int remainingTreasures;
    static int visitedNodes = 0;

    static LinkedList<int[]> treasures = new LinkedList<int[]>();


    public static void resetVisited() {
        visitedMatrix = new boolean[graphMatrix.length][graphMatrix[0].length];
    }

    public static double[][] prepareScoreArrays() {
		double[][] hCost = new double[graphMatrix.length][graphMatrix[0].length];
		for (int i = 0; i < graphMatrix.length; i++)
            for (int j = 0; j < graphMatrix[i].length; j++)
    			hCost[i][j] = Double.MAX_VALUE;
        return hCost;
    }

	public static Common.Node getPathAStar(int x, int y) {
        LinkedList<Common.Node> open = new LinkedList<Common.Node>();
		double[][] hCost = prepareScoreArrays();
		double[][] fScore = prepareScoreArrays();
		double[][] gScore = prepareScoreArrays();
        hCost[x][y] = 0;
        fScore[x][y] = 0;
        gScore[x][y] = 0;
		
		open.add(new Common.Node(x, y, null, 0));
		//System.out.println("Odpiram vozlisce " + open.peek().toString());
		
		while(!open.isEmpty()){
			double minVal = Double.MAX_VALUE;
			int minPos = 0;
			Common.Node n = null;
            double tempGScore;
			
			for (int i = 0; i < open.size(); i++) {
				Common.Node node = open.get(i);
				if (fScore[node.x][node.y] <= minVal) {
					minVal = fScore[node.x][node.y];
					minPos = i;
					n = node;
				}
			}

			open.remove(minPos);
			visitedMatrix[n.x][n.y] = true;
            visitedNodes++;
			//System.out.println("Vozlisce " + n);

            if (Common.isTreasure(n.x, n.y, graphMatrix)) {
                remainingTreasures--;
                graphMatrix[n.x][n.y] = 4;
                resetVisited();
                Common.Node nTemp = n.getParent();
                while(nTemp.getParent().getParent() != null) {
                    hCost[nTemp.x][nTemp.y] = Double.MAX_VALUE;
                    fScore[nTemp.x][nTemp.y] = Double.MAX_VALUE;
                    gScore[nTemp.x][nTemp.y] = Double.MAX_VALUE;
                    nTemp = nTemp.getParent();
                }
                for (Common.Node nThis : open) {
                    while(nThis.getParent().getParent() != null) {
                        hCost[nThis.x][nThis.y] = Double.MAX_VALUE;
                        fScore[nThis.x][nThis.y] = Double.MAX_VALUE;
                        gScore[nThis.x][nThis.y] = Double.MAX_VALUE;
                        nThis = nThis.getParent();
                    }
                }
                open.clear();
                //System.out.println("Remaining treasures: " + remainingTreasures);
            }

            if (Common.isEnd(n.x, n.y, graphMatrix) && remainingTreasures == 0) {
                //System.out.println("Exit is reached!");
                return n;
            }

            tempGScore = gScore[n.x][n.y] + graphMatrix[n.x][n.y] + getDistanceToClosestGoal(n.x, n.y);
            //pogleda vozlišče na desni
            if(Common.isFree(n.x+1,n.y, graphMatrix, visitedMatrix)) {
                open.push(new Common.Node(n.x+1, n.y, n, originalMatrix[n.x+1][n.y]));
                if(tempGScore >= gScore[n.x+1][n.y]){}
                else {
                    gScore[n.x+1][n.y] = tempGScore;
                    fScore[n.x+1][n.y] = graphMatrix[n.x+1][n.y] + getDistanceToClosestGoal(n.x+1, n.y);
                }
            }

            //pogleda vozlišče na levi
            if(Common.isFree(n.x-1,n.y, graphMatrix, visitedMatrix)) {
                open.push(new Common.Node(n.x-1, n.y, n, originalMatrix[n.x-1][n.y]));
                if(tempGScore >= gScore[n.x-1][n.y]){}
                else{
                    gScore[n.x-1][n.y] = tempGScore;
                    fScore[n.x-1][n.y] = graphMatrix[n.x-1][n.y] + getDistanceToClosestGoal(n.x-1, n.y);
                }
            }

            //pogleda vozlišče pod
            if(Common.isFree(n.x,n.y+1, graphMatrix, visitedMatrix)) {
                open.push(new Common.Node(n.x, n.y+1, n, originalMatrix[n.x][n.y+1]));
                if(tempGScore >= gScore[n.x][n.y+1]){}
                else{
                    gScore[n.x][n.y+1] = tempGScore;
                    fScore[n.x][n.y+1] = graphMatrix[n.x][n.y+1] + getDistanceToClosestGoal(n.x, n.y+1);
                }
            }

            //pogleda vozlišče nad
            if(Common.isFree(n.x,n.y-1,graphMatrix, visitedMatrix)) {
                open.push(new Common.Node(n.x, n.y-1, n, originalMatrix[n.x][n.y-1]));
                if(tempGScore >= gScore[n.x][n.y-1]){}
                else{
                    gScore[n.x][n.y-1] = tempGScore;
                    fScore[n.x][n.y-1] = graphMatrix[n.x][n.y-1] + getDistanceToClosestGoal(n.x, n.y-1);
                }
            }
		}
        return null;
	}

    public static double getDistanceToClosestGoal(int x, int y){
        int dx, dy;
        if(remainingTreasures != 0){
            double temp, lowestValue = Double.POSITIVE_INFINITY;
            for (int i = 0; i < treasures.size(); i++) {
                dx = Math.abs(x - treasures.get(i)[0]);
                dy = Math.abs(y - treasures.get(i)[1]);
                //temp = 1 * (dx + dy) + (Math.sqrt(2) - 2 * 1) * Math.min(dx, dy);
                temp = dx + dy;
                if(lowestValue > temp)
                    lowestValue = temp;
            }
            
            return lowestValue;
        }
        dx = Math.abs(x - endX);
        dy = Math.abs(y - endY);
        return dx + dy;
        //return 1 * (dx + dy) + (Math.sqrt(2) - 2 * 1) * Math.min(dx, dy);
    }

    public static void main(String[] args) {
        List<Object> com = Common.value();
        graphMatrix = (int[][]) com.get(0);
        remainingTreasures = (int) com.get(1);
        startX = (int) com.get(2);
        startY = (int) com.get(3);

        originalMatrix = (int[][]) com.get(0);

/*
        System.out.println("Number of treasures: " + allTreasures);
        for (int i = 0; i < graphMatrix.length; i++) {
            for (int j = 0; j < graphMatrix[i].length; j++) {
                System.out.printf("%2d", graphMatrix[i][j]);
            }
            System.out.println();
        }
 */
        resetVisited();
        Common.Node n = getPathAStar(startX, startY);

        //System.out.println(n);
        //graphMatrix[n.x][n.y] = 0;
        int[][] value = (int[][]) Common.value().get(0);

        Common.printFinalPath(n, startX, startY, visitedNodes);

        while(n.getParent().getParent() != null) {
            //System.out.println(n.getParent());
            n = n.getParent();
            if(value[n.x][n.y] != -3)
                value[n.x][n.y] = 0;
        }

        Izris.drawOut(value);
    }
}